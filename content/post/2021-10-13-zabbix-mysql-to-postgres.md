---
title: How I migrated zabbix (5.0) db from mysql to postgres 
date: 2021-10-13
tags: ["zabbix", "mysql", "postgresql", "mariadb", "postgres"]
---



```
postgres@db:~$ createuser zabbix
postgres@db:~$ dropdb zabbix ; createdb -O zabbix zabbix  ; zcat /tmp/schema.sql.gz | psql -h db -p 5433 zabbix zabbix
postgres@db:~$ pgloader -v pgloader.zabbix.command_01
postgres@db:~$ pgloader -v pgloader.zabbix.command_02
postgres@db:~$ pgloader -v pgloader.zabbix.command_03
postgres@db:~$ pgloader -v pgloader.zabbix.command_04
postgres@db:~$ pgloader -v pgloader.zabbix.command_05
```
---------
```
$ cat pgloader.zabbix.command_01 
LOAD DATABASE
     FROM mysql://zabbix@localhost/zabbix
     INTO postgresql://zabbix@localhost:5433/zabbix

INCLUDING ONLY TABLE NAMES MATCHING 'hosts', 'users'

WITH include no drop,
     create no tables,
     create no indexes, 
     reset sequences, 
     no foreign keys,
     preserve index names,
     data only

SET MySQL PARAMETERS
net_read_timeout = '900',
net_write_timeout = '900'

ALTER SCHEMA 'zabbix' RENAME TO 'public'
;
```
```
$ cat pgloader.zabbix.command_02 
LOAD DATABASE
     FROM mysql://zabbix@localhost/zabbix
     INTO postgresql://zabbix@localhost:5433/zabbix

INCLUDING ONLY TABLE NAMES MATCHING 'applications', 'valuemaps', 'interface', 'actions', 'events', 'media_type', 'triggers', 'slideshows', 'images', 'dashboard', 'drules'

WITH include no drop,
     create no tables,
     create no indexes, 
     reset sequences, 
     no foreign keys,
     preserve index names,
     data only

SET MySQL PARAMETERS
net_read_timeout = '900',
net_write_timeout = '900'

ALTER SCHEMA 'zabbix' RENAME TO 'public'
;
```
```
$ cat pgloader.zabbix.command_03
LOAD DATABASE
     FROM mysql://zabbix@localhost/zabbix
     INTO postgresql://zabbix@localhost:5433/zabbix

INCLUDING ONLY TABLE NAMES MATCHING 'auditlog', 'usrgrp', 'hstgrp', 'dhosts', 'dchecks', 'regexps', 'items', 'httptest', 'icon_map', 'widget'

WITH include no drop,
     create no tables,
     create no indexes, 
     reset sequences, 
     no foreign keys,
     preserve index names,
     data only

SET MySQL PARAMETERS
net_read_timeout = '900',
net_write_timeout = '900'

ALTER SCHEMA 'zabbix' RENAME TO 'public'
;
```
```
$ cat pgloader.zabbix.command_04
LOAD DATABASE
     FROM mysql://zabbix@localhost/zabbix
     INTO postgresql://zabbix@localhost:5433/zabbix

INCLUDING ONLY TABLE NAMES MATCHING 'httpstep', 'sysmaps', 'dservices'

WITH include no drop,
     create no tables,
     create no indexes, 
     reset sequences, 
     no foreign keys,
     preserve index names,
     data only

SET MySQL PARAMETERS
net_read_timeout = '900',
net_write_timeout = '900'

ALTER SCHEMA 'zabbix' RENAME TO 'public'
;
```
```
$ cat pgloader.zabbix.command_05 
LOAD DATABASE
     FROM mysql://zabbix@localhost/zabbix
     INTO postgresql://zabbix@localhost:5433/zabbix

excluding table names matching 'hosts', 'users', 'applications', 'auditlog', 'usrgrp', 'hstgrp', 'regexps', 'items', 'valuemaps', 'interface', 'actions', 'events', 'media_type', 'triggers', 'httptest', 'slideshows', 'httpstep', 'images', 'icon_map', 'sysmaps', 'dashboard', 'widget', 'drules', 'dhosts', 'dservices', 
'dchecks'

WITH include no drop,
     create no tables,
     create no indexes, 
     reset sequences, 
     no foreign keys,
     preserve index names,
     data only

SET MySQL PARAMETERS
net_read_timeout = '900',
net_write_timeout = '900'

ALTER SCHEMA 'zabbix' RENAME TO 'public'
;
```
