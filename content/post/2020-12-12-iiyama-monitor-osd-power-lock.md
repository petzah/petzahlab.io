---
title: Lock/unlock OSD/Power on iiyama monitor
date: 2020-12-12
tags: ["iiyama", "monitor", "osd", "power"]
---

This works for  ProLite XUB2595WSU-B1:

**To lock OSD:**<br>
Have monitor turned off. Press and hold "Menu" and turn monitor on.

**To unlock OSD:**<br>
Have monitor turned off. Press and hold "Menu" and turn monitor on.

**To lock Power:**<br>
Have monitor turned on, press and hold "Menu" button for 10 seconds.

**To unlock Power:**<br>
Have monitor turned on (can't have it off anyway as it is locked :D ), press and hold "Menu" button together with Power button for 10 seconds.
